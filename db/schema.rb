# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150407185351) do

  create_table "campos", force: :cascade do |t|
    t.integer  "lay_id",     limit: 4
    t.integer  "campo_id",   limit: 4
    t.string   "nombre",     limit: 255
    t.integer  "posicion",   limit: 4
    t.integer  "longitud",   limit: 4
    t.integer  "tipo",       limit: 4
    t.integer  "nivel",      limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "pruebas", force: :cascade do |t|
    t.string   "texto",      limit: 255
    t.date     "fecha"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "reglas", force: :cascade do |t|
    t.integer  "regla_id",      limit: 4
    t.string   "descripcion",   limit: 255
    t.boolean  "activo",        limit: 1
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "todos",         limit: 1
    t.string   "dias",          limit: 255
    t.date     "fecha_inicial"
    t.date     "fecha_final"
    t.boolean  "vigencia",      limit: 1
    t.integer  "maximo",        limit: 4
    t.integer  "minimo",        limit: 4
    t.integer  "contador",      limit: 4
  end

  create_table "tactecorreoxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",       limit: 12, null: false
    t.integer "Id_Condicion",     limit: 2,  null: false
    t.integer "Bit_Envia_Correo", limit: 2
    t.integer "Id_campo",         limit: 2
    t.integer "Id_campo_rel",     limit: 2
  end

  create_table "tactecorreoxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",       limit: 12, null: false
    t.integer "Id_Condicion",     limit: 2,  null: false
    t.integer "Bit_Envia_Correo", limit: 2
    t.integer "Id_campo",         limit: 2
    t.integer "Id_campo_rel",     limit: 2
  end

  create_table "tactesms", primary_key: "Id_Condicion", force: :cascade do |t|
    t.integer "Bit_Envia_SMS", limit: 2
    t.integer "Id_campo",      limit: 2
    t.integer "Id_campo_rel",  limit: 2
  end

  create_table "tactesmsxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",    limit: 12, null: false
    t.integer "Id_Condicion",  limit: 2,  null: false
    t.integer "Bit_Envia_SMS", limit: 2
    t.integer "Id_campo",      limit: 2
    t.integer "Id_campo_rel",  limit: 2
  end

  create_table "tactesmsxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",    limit: 12, null: false
    t.integer "Id_Condicion",  limit: 2,  null: false
    t.integer "Bit_Envia_SMS", limit: 2
    t.integer "Id_campo",      limit: 2
    t.integer "Id_campo_rel",  limit: 2
  end

  create_table "tactival", id: false, force: :cascade do |t|
    t.integer "Id_Condicion", limit: 2,   null: false
    t.integer "Id_Horario",   limit: 4,   null: false
    t.integer "Num_Alarma",   limit: 2
    t.string  "Hora_Ini",     limit: 4
    t.string  "Hora_Fin",     limit: 4
    t.integer "Limite_Tran",  limit: 2
    t.integer "Minimo_Tran",  limit: 2
    t.integer "Intermedio",   limit: 4
    t.integer "Intervalo",    limit: 4
    t.integer "UMIntermedio", limit: 4
    t.integer "UMIntervalo",  limit: 4
    t.integer "TimeOut",      limit: 4
    t.integer "UMTimeOut",    limit: 4
    t.string  "TemplateRef",  limit: 255
  end

  create_table "tactvcorreo", id: false, force: :cascade do |t|
    t.integer "Id_Condicion", limit: 2,               null: false
    t.integer "Consecutivo",  limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",  limit: 250
    t.string  "Correo",       limit: 150
  end

  create_table "tactvcorreoxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,              null: false
    t.integer "Id_Condicion", limit: 2,               null: false
    t.integer "Consecutivo",  limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",  limit: 250
    t.string  "Correo",       limit: 150
  end

  create_table "tactvcorreoxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,              null: false
    t.integer "Id_Condicion", limit: 2,               null: false
    t.integer "Consecutivo",  limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",  limit: 250
    t.string  "Correo",       limit: 150
  end

  create_table "tactvsms", id: false, force: :cascade do |t|
    t.integer "Id_Condicion",   limit: 2,               null: false
    t.integer "Consecutivo",    limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",    limit: 250
    t.string  "Numero_Celular", limit: 15
  end

  create_table "tactvsmsxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12,              null: false
    t.integer "Id_Condicion",   limit: 2,               null: false
    t.integer "Consecutivo",    limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",    limit: 250
    t.string  "Numero_Celular", limit: 15
  end

  create_table "tactvsmsxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12,              null: false
    t.integer "Id_Condicion",   limit: 2,               null: false
    t.integer "Consecutivo",    limit: 2,   default: 0, null: false
    t.string  "Valor_Campo",    limit: 250
    t.string  "Numero_Celular", limit: 15
  end

  create_table "tactxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,  null: false
    t.integer "Id_Condicion", limit: 2,   null: false
    t.integer "Id_Horario",   limit: 2,   null: false
    t.integer "Num_Alarma",   limit: 2
    t.string  "Hora_Ini",     limit: 4
    t.string  "Hora_Fin",     limit: 4
    t.integer "Limite_Tran",  limit: 2
    t.integer "Minimo_Tran",  limit: 2
    t.integer "Intermedio",   limit: 4
    t.integer "Intervalo",    limit: 4
    t.integer "UMIntermedio", limit: 4
    t.integer "UMIntervalo",  limit: 4
    t.integer "TimeOut",      limit: 4
    t.integer "UMTimeOut",    limit: 4
    t.string  "TemplateRef",  limit: 255
  end

  create_table "tactxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,  null: false
    t.integer "Id_Condicion", limit: 2,   null: false
    t.integer "Id_Horario",   limit: 4,   null: false
    t.integer "Num_Alarma",   limit: 2
    t.string  "Hora_Ini",     limit: 4
    t.string  "Hora_Fin",     limit: 4
    t.integer "Limite_Tran",  limit: 2
    t.integer "Minimo_Tran",  limit: 2
    t.integer "Intermedio",   limit: 4
    t.integer "Intervalo",    limit: 4
    t.integer "UMIntermedio", limit: 4
    t.integer "UMIntervalo",  limit: 4
    t.integer "TimeOut",      limit: 4
    t.integer "UMTimeOut",    limit: 4
    t.string  "TemplateRef",  limit: 180
  end

  create_table "taplicac", primary_key: "Id_Aplicacion", force: :cascade do |t|
    t.integer  "Ssid",          limit: 2,          null: false
    t.text     "Descripcion",   limit: 4294967295
    t.text     "Ubicacion",     limit: 4294967295
    t.string   "Version",       limit: 10
    t.datetime "Fecha_Version"
  end

  add_index "taplicac", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree
  add_index "taplicac", ["Ssid"], name: "PSsid", using: :btree

  create_table "taplxgpo", id: false, force: :cascade do |t|
    t.integer "Id_Grupo",      limit: 2, null: false
    t.integer "Id_Aplicacion", limit: 2, null: false
    t.integer "Id_Objeto",     limit: 2, null: false
    t.integer "Prop_Enabled",  limit: 4
    t.integer "Prop_Visible",  limit: 2
  end

  add_index "taplxgpo", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree
  add_index "taplxgpo", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "taplxgpo", ["Id_Objeto"], name: "PId_Objeto", using: :btree

  create_table "taplxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",    limit: 12, null: false
    t.integer "Id_Aplicacion", limit: 2,  null: false
    t.integer "Id_Objeto",     limit: 2,  null: false
    t.integer "Prop_Enabled",  limit: 4
    t.integer "Prop_Visible",  limit: 4
  end

  add_index "taplxusr", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree
  add_index "taplxusr", ["Id_Objeto"], name: "PId_Objeto", using: :btree
  add_index "taplxusr", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tatrifil", id: false, force: :cascade do |t|
    t.integer "Num_Pantalla", limit: 2,  null: false
    t.integer "Num_Filtro",   limit: 2,  null: false
    t.integer "Num_Usuario",  limit: 4,  null: false
    t.integer "Id_condicion", limit: 2,  null: false
    t.integer "Tipo",         limit: 2,  null: false
    t.integer "Condicion",    limit: 2,  null: false
    t.integer "Conector",     limit: 2
    t.integer "Id_Campo",     limit: 2,  null: false
    t.integer "Longitud",     limit: 2
    t.integer "Negada",       limit: 1
    t.integer "Operador",     limit: 2
    t.integer "Parentesis",   limit: 2
    t.integer "Posicion",     limit: 2
    t.string  "Valor",        limit: 50
    t.string  "Descripcion",  limit: 50
    t.integer "Es_Visible",   limit: 1
    t.integer "Color_Fondo",  limit: 4
    t.integer "Color_Letra",  limit: 4
  end

  add_index "tatrifil", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tatrifil", ["Id_condicion"], name: "PId_condicion", using: :btree
  add_index "tatrifil", ["Num_Filtro"], name: "PNum_Filtro", using: :btree
  add_index "tatrifil", ["Num_Pantalla"], name: "PNum_Pantalla", using: :btree
  add_index "tatrifil", ["Num_Usuario"], name: "PNum_Usuario", using: :btree

  create_table "tatrxala", id: false, force: :cascade do |t|
    t.integer "Id_Condicion", limit: 2,  null: false
    t.integer "Id_Horario",   limit: 4,  null: false
    t.integer "Num_Alarma",   limit: 2,  null: false
    t.integer "Color_Letra",  limit: 4
    t.integer "Color_Fondo",  limit: 4
    t.string  "Mensaje",      limit: 50
  end

  create_table "tatrxmsg", id: false, force: :cascade do |t|
    t.integer "Id_Condicion",     limit: 2,    null: false
    t.integer "Id_Horario",       limit: 4,    null: false
    t.integer "Num_Alarma",       limit: 2,    null: false
    t.integer "Bit_Fecha_hora",   limit: 2,    null: false
    t.integer "Bit_Descripcion",  limit: 2,    null: false
    t.integer "Bit_escala",       limit: 2,    null: false
    t.integer "Bit_Limites",      limit: 2,    null: false
    t.integer "Bit_Referencias",  limit: 2,    null: false
    t.string  "Mensaje",          limit: 2000
    t.string  "Mensaje_En_Clave", limit: 2000
  end

  create_table "tatrxmsgxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",       limit: 12,   null: false
    t.integer "Id_Condicion",     limit: 2,    null: false
    t.integer "Id_Horario",       limit: 2,    null: false
    t.integer "Num_Alarma",       limit: 2,    null: false
    t.integer "Bit_Fecha_hora",   limit: 2,    null: false
    t.integer "Bit_Descripcion",  limit: 2,    null: false
    t.integer "Bit_escala",       limit: 2,    null: false
    t.integer "Bit_Limites",      limit: 2,    null: false
    t.integer "Bit_Referencias",  limit: 2,    null: false
    t.string  "Mensaje",          limit: 2000
    t.string  "Mensaje_En_Clave", limit: 2000
  end

  create_table "tatrxmsgxusrestacion", id: false, force: :cascade do |t|
    t.string  "id_Usuario",       limit: 12,   null: false
    t.integer "Id_Condicion",     limit: 2,    null: false
    t.integer "Id_Horario",       limit: 2,    null: false
    t.integer "Num_Alarma",       limit: 2,    null: false
    t.integer "Bit_Fecha_hora",   limit: 2,    null: false
    t.integer "Bit_Descripcion",  limit: 2,    null: false
    t.integer "Bit_escala",       limit: 2,    null: false
    t.integer "Bit_Limites",      limit: 2,    null: false
    t.integer "Bit_Referencias",  limit: 2,    null: false
    t.string  "Mensaje",          limit: 2000
    t.string  "Mensaje_En_Clave", limit: 2000
  end

  create_table "tatrxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.integer "Id_Horario",   limit: 2,  null: false
    t.integer "Num_Alarma",   limit: 2,  null: false
    t.integer "Color_Letra",  limit: 4
    t.integer "Color_Fondo",  limit: 4
    t.string  "Mensaje",      limit: 50
  end

  create_table "tatrxusrestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.integer "Id_Horario",   limit: 4,  null: false
    t.integer "Num_Alarma",   limit: 2,  null: false
    t.integer "Color_Letra",  limit: 4
    t.integer "Color_Fondo",  limit: 4
    t.string  "Mensaje",      limit: 50
  end

  create_table "tauthent", primary_key: "Id", force: :cascade do |t|
    t.text "Mascara_Tandem", limit: 4294967295
  end

  add_index "tauthent", ["Id"], name: "PId", using: :btree

  create_table "tbitalarma", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",         limit: 12,         null: false
    t.integer "Id_Lay",             limit: 4,          null: false
    t.integer "Id_Formato",         limit: 4,          null: false
    t.integer "Id_Aplicacion",      limit: 4,          null: false
    t.integer "Id_TipoReg",         limit: 4,          null: false
    t.string  "IP_Adress",          limit: 50,         null: false
    t.string  "PC_Name",            limit: 50,         null: false
    t.string  "Fecha_Hora",         limit: 30,         null: false
    t.integer "Id_Filtro",          limit: 4
    t.string  "Descripcion_Filtro", limit: 50
    t.string  "Escala",             limit: 50
    t.text    "mensaje",            limit: 4294967295
    t.string  "Limite",             limit: 50
    t.string  "Tiempo",             limit: 50
    t.text    "REFERENCIA",         limit: 4294967295
    t.integer "Status",             limit: 4
    t.text    "txtcondicion",       limit: 4294967295
  end

  add_index "tbitalarma", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree
  add_index "tbitalarma", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tbitalarma", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tbitalarma", ["Id_TipoReg"], name: "PId_TipoReg", using: :btree
  add_index "tbitalarma", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tbitctrl", primary_key: "Id_TipoReg", force: :cascade do |t|
    t.integer "Periodo",      limit: 2
    t.integer "UnidadTiempo", limit: 2
    t.integer "Activo",       limit: 2
  end

  create_table "tbitfile", primary_key: "ID_CONDICION", force: :cascade do |t|
    t.integer "TIPO",        limit: 4,  null: false
    t.string  "FILETOWRITE", limit: 50
  end

  create_table "tbitfusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "ID_CONDICION", limit: 4,  null: false
    t.integer "TIPO",         limit: 4
    t.string  "FILETOWRITE",  limit: 50
  end

  create_table "tbitobse", id: false, force: :cascade do |t|
    t.string   "Id_Usuario",          limit: 12,         null: false
    t.integer  "Id_Lay",              limit: 4,          null: false
    t.integer  "Id_Formato",          limit: 4,          null: false
    t.integer  "Id_Aplicacion",       limit: 4,          null: false
    t.integer  "Id_TipoReg",          limit: 4,          null: false
    t.string   "IP_Adress",           limit: 50,         null: false
    t.string   "PC_Name",             limit: 50,         null: false
    t.datetime "Fecha_Hora",                             null: false
    t.datetime "Fecha_Hora_Atencion",                    null: false
    t.integer  "Id_Usuario_Atencion", limit: 4,          null: false
    t.text     "Observaciones",       limit: 4294967295
    t.string   "Status",              limit: 50
    t.datetime "Fecha_Hora_Mod"
  end

  add_index "tbitobse", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree
  add_index "tbitobse", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tbitobse", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tbitobse", ["Id_TipoReg"], name: "PId_TipoReg", using: :btree
  add_index "tbitobse", ["Id_Usuario"], name: "PId_Usuario", using: :btree
  add_index "tbitobse", ["Id_Usuario_Atencion"], name: "PId_Usuario_Atencion", using: :btree

  create_table "tbl_cattipocambio", id: false, force: :cascade do |t|
    t.string   "TC_FIID_BANCO",       limit: 4,  null: false
    t.datetime "TC_DIA_CAPTURADO",               null: false
    t.integer  "TC_CDE_MONEDA",       limit: 2,  null: false
    t.string   "TC_NOMBRE",           limit: 24, null: false
    t.string   "TC_TIPO_DE_CAMBIO",   limit: 7,  null: false
    t.datetime "TC_FECHA_DE_CAPTURA",            null: false
    t.string   "TC_ESTADO",           limit: 1
  end

  create_table "tcolxdet", id: false, force: :cascade do |t|
    t.integer "Id_Lay",         limit: 2,   null: false
    t.integer "Id_Formato",     limit: 2,   null: false
    t.integer "Id_Grupo",       limit: 2,   null: false
    t.integer "Columna",        limit: 2,   null: false
    t.integer "Tipo_Campo",     limit: 2
    t.integer "Id_Campo",       limit: 2
    t.string  "Titulo",         limit: 25
    t.string  "Auxiliar_Campo", limit: 200
    t.integer "Se_Suma",        limit: 2
  end

  add_index "tcolxdet", ["Columna"], name: "PColumna", using: :btree
  add_index "tcolxdet", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tcolxdet", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tcolxdet", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tcolxgpo", id: false, force: :cascade do |t|
    t.integer "Id_Lay",         limit: 2,   null: false
    t.integer "Id_Formato",     limit: 2,   null: false
    t.integer "Id_Grupo",       limit: 2,   null: false
    t.integer "Columna",        limit: 2,   null: false
    t.string  "Titulo",         limit: 30
    t.integer "Tipo_Campo",     limit: 2
    t.integer "Id_Campo",       limit: 2
    t.string  "Auxiliar_Campo", limit: 200
    t.integer "Tipo_Grafica",   limit: 2
  end

  add_index "tcolxgpo", ["Columna"], name: "PColumna", using: :btree
  add_index "tcolxgpo", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tcolxgpo", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tcolxgpo", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tconalar", id: false, force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,  null: false
    t.integer "Id_Formato",   limit: 2,  null: false
    t.integer "Tipo",         limit: 2,  null: false
    t.integer "Num_Filtro",   limit: 2,  null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.string  "Descripcion",  limit: 30
    t.integer "Color_Fondo",  limit: 4
    t.integer "Color_Letra",  limit: 4
  end

  add_index "tconalar", ["Id_Condicion"], name: "PId_Condicion", using: :btree

  create_table "tconalarestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Lay",       limit: 2,  null: false
    t.integer "Id_Formato",   limit: 2,  null: false
    t.integer "Tipo",         limit: 2,  null: false
    t.integer "Num_Filtro",   limit: 2,  null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.string  "Descripcion",  limit: 30
    t.integer "Color_Fondo",  limit: 4
    t.integer "Color_Letra",  limit: 4
  end

  add_index "tconalarestacion", ["Id_Condicion"], name: "PId_Condicion", using: :btree
  add_index "tconalarestacion", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tconalarfiltros", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.integer "Id_Formato",  limit: 2,  null: false
    t.integer "Num_Filtro",  limit: 2,  null: false
    t.integer "Puerto",      limit: 2
    t.string  "Descripcion", limit: 30
  end

  add_index "tconalarfiltros", ["Num_Filtro"], name: "PNum_Filtro", using: :btree

  create_table "tconfig", primary_key: "Id_Config", force: :cascade do |t|
    t.string "Desc_Config", limit: 50
  end

  add_index "tconfig", ["Id_Config"], name: "PId_Config", using: :btree

  create_table "tconsexis", id: false, force: :cascade do |t|
    t.integer "Id_Formato", limit: 4,          null: false
    t.integer "Id_Lay",     limit: 4,          null: false
    t.string  "Id_Nombre",  limit: 50,         null: false
    t.text    "Id_Query",   limit: 4294967295
  end

  add_index "tconsexis", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tconsexis", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tconsexis", ["Id_Nombre"], name: "PId_Nombre", using: :btree

  create_table "tcontrol", primary_key: "Fecha", force: :cascade do |t|
  end

  create_table "tdeflogs", primary_key: "Id_Log", force: :cascade do |t|
    t.string  "Descripcion",         limit: 60
    t.string  "Nodo",                limit: 8
    t.string  "Volumen",             limit: 8
    t.string  "Subvol",              limit: 8
    t.string  "Nombre_Archivo",      limit: 8
    t.string  "Posicion_Llave",      limit: 80
    t.integer "Long_Posicion_Llave", limit: 2
    t.string  "Masc_Volumen",        limit: 20
    t.string  "Masc_Subvol",         limit: 20
    t.string  "Masc_Nomb_Arch",      limit: 20
    t.boolean "Es_Dinamico",         limit: 1
    t.boolean "Es_Activo",           limit: 1
  end

  add_index "tdeflogs", ["Id_Log"], name: "PId_Log", using: :btree

  create_table "tdesfiltrosa", id: false, force: :cascade do |t|
    t.integer "IdFiltro",   limit: 4,    null: false
    t.integer "IdCond",     limit: 4,    null: false
    t.string  "Condicion",  limit: 255,  null: false
    t.string  "Operador",   limit: 45,   null: false
    t.string  "Valor1",     limit: 255,  null: false
    t.string  "Parentesis", limit: 45,   null: false
    t.string  "Negacion",   limit: 45,   null: false
    t.string  "Valor2",     limit: 255,  null: false
    t.string  "Campo",      limit: 45,   null: false
    t.string  "Conector",   limit: 45,   null: false
    t.string  "Query",      limit: 1000, null: false
    t.string  "TipoDato",   limit: 45,   null: false
  end

  create_table "tdetdcon", id: false, force: :cascade do |t|
    t.integer "Id_Condicion",  limit: 2,  null: false
    t.integer "Num_Condicion", limit: 2,  null: false
    t.integer "Conector",      limit: 2
    t.integer "Id_Campo",      limit: 2,  null: false
    t.integer "Longitud",      limit: 2
    t.integer "Negada",        limit: 2
    t.integer "Operador",      limit: 2
    t.integer "Parentesis",    limit: 2
    t.integer "Posicion",      limit: 2
    t.string  "Valor",         limit: 25
  end

  add_index "tdetdcon", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tdetdcon", ["Id_Condicion"], name: "PId_Condicion", using: :btree

  create_table "tdetdconestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",    limit: 12, null: false
    t.integer "Id_Condicion",  limit: 2,  null: false
    t.integer "Num_Condicion", limit: 2,  null: false
    t.integer "Conector",      limit: 2
    t.integer "Id_Campo",      limit: 2,  null: false
    t.integer "Longitud",      limit: 2
    t.integer "Negada",        limit: 2
    t.integer "Operador",      limit: 2
    t.integer "Parentesis",    limit: 2
    t.integer "Posicion",      limit: 2
    t.string  "Valor",         limit: 25
  end

  add_index "tdetdconestacion", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tdetdconestacion", ["Id_Condicion"], name: "PId_Condicion", using: :btree
  add_index "tdetdconestacion", ["Id_Usuario"], name: "PId_Usuario", using: :btree
  add_index "tdetdconestacion", ["Num_Condicion"], name: "PNum_Condicion", using: :btree

  create_table "tdetdconfiltros", id: false, force: :cascade do |t|
    t.integer "Num_Filtro",    limit: 2,  null: false
    t.integer "Num_Condicion", limit: 2,  null: false
    t.integer "Conector",      limit: 2
    t.integer "Id_Campo",      limit: 2
    t.integer "Longitud",      limit: 2
    t.integer "Negada",        limit: 2
    t.integer "Operador",      limit: 2
    t.integer "Parentesis",    limit: 2
    t.integer "Posicion",      limit: 2
    t.string  "Valor",         limit: 25
  end

  add_index "tdetdconfiltros", ["Num_Condicion"], name: "PNum_Condicion", using: :btree
  add_index "tdetdconfiltros", ["Num_Filtro"], name: "PNum_Filtro", using: :btree

  create_table "tdtokens", id: false, force: :cascade do |t|
    t.integer "Id_Lay",          limit: 2,  null: false
    t.string  "Id_Token",        limit: 5,  null: false
    t.integer "Id_Campo",        limit: 2,  null: false
    t.string  "Nombre",          limit: 50
    t.integer "Longitud",        limit: 2
    t.integer "Tipo_dato",       limit: 2
    t.integer "Long_Despliegue", limit: 2
    t.string  "Alias",           limit: 50
    t.integer "Posicion",        limit: 4
  end

  create_table "temaxala", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.integer "Id_Horario",   limit: 2,  null: false
    t.integer "Num_Alarma",   limit: 2,  null: false
    t.integer "Id_Email",     limit: 2,  null: false
  end

  create_table "temaxalaestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Condicion", limit: 2,  null: false
    t.integer "Id_Horario",   limit: 4,  null: false
    t.integer "Num_Alarma",   limit: 2,  null: false
    t.integer "Id_Email",     limit: 2,  null: false
  end

  create_table "tequipos", primary_key: "Id_Equipo", force: :cascade do |t|
    t.string "Desc_Equipo", limit: 50
    t.string "IP_Equipo",   limit: 50
  end

  add_index "tequipos", ["Id_Equipo"], name: "PId_Equipo", using: :btree

  create_table "tescalamientosalertas", id: false, force: :cascade do |t|
    t.integer "Id_Aplicacion", limit: 4,   default: 0,  null: false
    t.integer "Id_Estatus",    limit: 4,   default: 0,  null: false
    t.integer "Nivel",         limit: 4,   default: 0,  null: false
    t.string  "Mensaje",       limit: 255, default: "", null: false
    t.string  "Mails",         limit: 300, default: "", null: false
    t.integer "Tiempo",        limit: 4,   default: 0,  null: false
    t.integer "Unidad",        limit: 4,   default: 0,  null: false
  end

  create_table "testatusalarma", id: false, force: :cascade do |t|
    t.integer "Id_Estatus",     limit: 4,                null: false
    t.integer "Aplicacion",     limit: 4,   default: 0,  null: false
    t.string  "Id_Descripcion", limit: 50,  default: "", null: false
    t.string  "ColorFondo",     limit: 50
    t.string  "ColorLetra",     limit: 50
    t.string  "Visible",        limit: 255
    t.integer "Orden",          limit: 4
  end

  create_table "tfiltrosa", id: false, force: :cascade do |t|
    t.integer "IDFiltro",    limit: 4,  null: false
    t.string  "Descripcion", limit: 50
    t.integer "Formato",     limit: 2
    t.integer "LayOut",      limit: 2
  end

  add_index "tfiltrosa", ["IDFiltro"], name: "PIDFiltro", using: :btree

  create_table "tfilxgpo", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12, null: false
    t.integer "Id_Formato",   limit: 4,  null: false
    t.integer "Id_Condicion", limit: 4,  null: false
    t.integer "Id_Lay",       limit: 4,  null: false
    t.integer "Id_Campo",     limit: 4,  null: false
    t.string  "Nombre_Campo", limit: 50
    t.integer "Posicion",     limit: 4
    t.integer "Longitud",     limit: 4
    t.integer "Tipo_Dato",    limit: 4
    t.integer "Nivel",        limit: 4
    t.integer "Se_Sustituye", limit: 1
  end

  add_index "tfilxgpo", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tfilxgpo", ["Id_Campo"], name: "PId_CampoG", using: :btree
  add_index "tfilxgpo", ["Id_Condicion"], name: "PId_Condicion", using: :btree
  add_index "tfilxgpo", ["Id_Condicion"], name: "PId_CondicionG", using: :btree
  add_index "tfilxgpo", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tfilxgpo", ["Id_Formato"], name: "PId_FormatoG", using: :btree
  add_index "tfilxgpo", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tfilxgpo", ["Id_Lay"], name: "PId_LayG", using: :btree
  add_index "tfilxgpo", ["Id_Usuario"], name: "PId_Usuario", using: :btree
  add_index "tfilxgpo", ["Id_Usuario"], name: "PId_UsuarioG", using: :btree

  create_table "tfilxgpoestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,             null: false
    t.integer "Id_Formato",   limit: 4,              null: false
    t.integer "Id_Condicion", limit: 4,  default: 0, null: false
    t.integer "Id_Lay",       limit: 4,  default: 0, null: false
    t.integer "Id_Campo",     limit: 4,              null: false
    t.string  "Nombre_Campo", limit: 50
    t.integer "Posicion",     limit: 4
    t.integer "Longitud",     limit: 4
    t.integer "Tipo_Dato",    limit: 4
    t.integer "Nivel",        limit: 4
    t.integer "Se_Sustituye", limit: 1
  end

  add_index "tfilxgpoestacion", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tfilxgpoestacion", ["Id_Condicion"], name: "PId_Condicion", using: :btree
  add_index "tfilxgpoestacion", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tfilxgpoestacion", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tfilxgpoestacion", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tfmtxgpo", id: false, force: :cascade do |t|
    t.integer "Id_Grupo",   limit: 2, null: false
    t.integer "Id_Lay",     limit: 2, null: false
    t.integer "Id_Formato", limit: 2, null: false
  end

  add_index "tfmtxgpo", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tfmtxgpo", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tfmtxgpo", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tfmtxusr", id: false, force: :cascade do |t|
    t.string  "Id_Usuario", limit: 12,         null: false
    t.integer "Id_Lay",     limit: 2,          null: false
    t.integer "Id_Formato", limit: 2,          null: false
    t.text    "Mascara",    limit: 4294967295
  end

  add_index "tfmtxusr", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tfmtxusr", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tfmtxusr", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tformulas", id: false, force: :cascade do |t|
    t.integer "IdFormula",    limit: 4
    t.string  "IDUsuario",    limit: 10
    t.string  "Descripcion",  limit: 500
    t.string  "Query",        limit: 1000
    t.integer "Formato",      limit: 4
    t.integer "Layout",       limit: 4
    t.string  "TipoBaseTran", limit: 100
  end

  create_table "tforxcol", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12,         null: false
    t.integer "Id_Lay",         limit: 2,          null: false
    t.integer "Id_Formato",     limit: 2,          null: false
    t.integer "Num_Columna",    limit: 2,          null: false
    t.string  "Descripcion",    limit: 50
    t.text    "Formula",        limit: 4294967295
    t.text    "FormulaEnClave", limit: 4294967295
    t.string  "Formato",        limit: 50,         null: false
  end

  add_index "tforxcol", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tforxcol", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tforxcol", ["Id_Usuario"], name: "PId_Usuario", using: :btree
  add_index "tforxcol", ["Num_Columna"], name: "PNum_Columna", using: :btree

  create_table "tgpodlay", id: false, force: :cascade do |t|
    t.integer "Id_Lay",     limit: 2, null: false
    t.integer "Id_Formato", limit: 2, null: false
    t.integer "Id_Grupo",   limit: 2, null: false
    t.integer "Num_Nivel",  limit: 2, null: false
    t.integer "Id_Campo",   limit: 2
  end

  add_index "tgpodlay", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tgpodlay", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tgpodlay", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tgpodlay", ["Num_Nivel"], name: "PNum_Nivel", using: :btree

  create_table "tgposctrl", id: false, force: :cascade do |t|
    t.integer "Id_Lay",                 limit: 4,  default: 0, null: false
    t.integer "Id_Formato",             limit: 4,  default: 0, null: false
    t.integer "Id_Grupo",               limit: 4,  default: 0, null: false
    t.integer "Periodo",                limit: 4
    t.integer "UnidadTiempo",           limit: 4
    t.string  "UltimoArchivoEliminado", limit: 50
    t.integer "Activo",                 limit: 4
  end

  create_table "tgposlay", id: false, force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,  null: false
    t.integer "Id_Formato",   limit: 2,  null: false
    t.integer "Id_Grupo",     limit: 2,  null: false
    t.string  "Nombre_Grupo", limit: 60
  end

  add_index "tgposlay", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tgposlay", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tgposlay", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tgpoxusr", primary_key: "Id_Grupo", force: :cascade do |t|
    t.string  "Descripcion", limit: 60
    t.string  "Id_Usuario",  limit: 12, null: false
    t.integer "Estado",      limit: 2
  end

  add_index "tgpoxusr", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tgpoxusr", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tgrupos", primary_key: "Id_Grupo", force: :cascade do |t|
    t.string "Desc_Grupo", limit: 50
  end

  add_index "tgrupos", ["Id_Grupo"], name: "PId_Grupo", using: :btree

  create_table "thistorico", id: false, force: :cascade do |t|
    t.string   "Id_Usuario", limit: 12,  null: false
    t.datetime "Fecha",                  null: false
    t.string   "Contrasena", limit: 150, null: false
  end

  create_table "tipcambio", id: false, force: :cascade do |t|
    t.datetime "Fecha_Tipo",             null: false
    t.float    "Valor_Tipo",  limit: 53
    t.integer  "Status_Tipo", limit: 2
    t.string   "crncy_cde",   limit: 3,  null: false
  end

  add_index "tipcambio", ["Fecha_Tipo"], name: "PFecha_Tipo", using: :btree
  add_index "tipcambio", ["crncy_cde"], name: "Pcrncy_cde", using: :btree

  create_table "tlaydlog", id: false, force: :cascade do |t|
    t.integer "Id_Log",      limit: 2,  null: false
    t.integer "Id_Lay",      limit: 2,  null: false
    t.boolean "Es_Activo",   limit: 1
    t.string  "Descripcion", limit: 60
  end

  add_index "tlaydlog", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tlaydlog", ["Id_Log"], name: "PId_Log", using: :btree

  create_table "tlayeqxc", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.integer "Id_Campo",    limit: 2,  null: false
    t.integer "Consecutivo", limit: 2,  null: false
    t.integer "Longitud",    limit: 2
    t.string  "Valor",       limit: 50, null: false
    t.string  "Equivalente", limit: 50
  end

  add_index "tlayeqxc", ["Consecutivo"], name: "PConsecutivo", using: :btree
  add_index "tlayeqxc", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tlayeqxc", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tlayeqxc", ["Valor"], name: "PValor", using: :btree

  create_table "tlayeqxcant", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.integer "Id_Campo",    limit: 2,  null: false
    t.integer "Consecutivo", limit: 2,  null: false
    t.integer "Longitud",    limit: 2
    t.string  "Valor",       limit: 50, null: false
    t.string  "Equivalente", limit: 50
  end

  add_index "tlayeqxcant", ["Consecutivo"], name: "PConsecutivo", using: :btree
  add_index "tlayeqxcant", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tlayeqxcant", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tlayeqxcant", ["Valor"], name: "PValor", using: :btree

  create_table "tlayeqxcpaso", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.integer "Id_Campo",    limit: 2,  null: false
    t.integer "Consecutivo", limit: 2,  null: false
    t.integer "Longitud",    limit: 2
    t.string  "Valor",       limit: 50, null: false
    t.string  "Equivalente", limit: 50
  end

  add_index "tlayeqxcpaso", ["Consecutivo"], name: "PConsecutivo", using: :btree
  add_index "tlayeqxcpaso", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tlayeqxcpaso", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tlayeqxcpaso", ["Valor"], name: "PValor", using: :btree

  create_table "tlayidxs", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,          null: false
    t.integer "Id_Formato",  limit: 4,          null: false
    t.integer "Id_Indice",   limit: 2,          null: false
    t.string  "Nombre",      limit: 50,         null: false
    t.string  "Descripcion", limit: 60,         null: false
    t.integer "Orden",       limit: 2
    t.text    "Mascara",     limit: 4294967295
  end

  add_index "tlayidxs", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tlayidxs", ["Id_Indice"], name: "PId_Indice", using: :btree
  add_index "tlayidxs", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tlaylogc", id: false, force: :cascade do |t|
    t.integer "Id_Lay",        limit: 2,  null: false
    t.integer "Id_Campo",      limit: 2,  null: false
    t.string  "Nombre_Campo",  limit: 36
    t.integer "Posicion",      limit: 2
    t.integer "Longitud",      limit: 2
    t.boolean "Tipo_Dato",     limit: 1
    t.integer "Nivel",         limit: 2
    t.integer "Padre",         limit: 2
    t.integer "Se_Sustituye",  limit: 1
    t.string  "Alias",         limit: 36
    t.integer "Camp_Longitud", limit: 2
  end

  add_index "tlaylogc", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "tlaylogc", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tlaylogt", primary_key: "Id_Lay", force: :cascade do |t|
    t.string  "Descripcion", limit: 60
    t.integer "Num_Campos",  limit: 2
    t.text    "Mascara",     limit: 4294967295
  end

  add_index "tlaylogt", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tlaymsgt", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,          null: false
    t.integer "Id_Formato",  limit: 2,          null: false
    t.string  "Descripcion", limit: 60
    t.text    "Mascara",     limit: 4294967295
  end

  add_index "tlaymsgt", ["Id_Formato"], name: "PId_Formato", using: :btree
  add_index "tlaymsgt", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "tlayposcol", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",  limit: 12, null: false
    t.integer "Id_Lay",      limit: 4,  null: false
    t.string  "Id_Campo",    limit: 50, null: false
    t.integer "Pos_Columna", limit: 4
    t.integer "Edo_Columna", limit: 2
  end

  create_table "tlayposcol2", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",  limit: 12, null: false
    t.integer "Id_Lay",      limit: 4,  null: false
    t.string  "Id_Campo",    limit: 50, null: false
    t.integer "Pos_Columna", limit: 4
    t.integer "Edo_Columna", limit: 2
    t.integer "APLICACION",  limit: 2
    t.integer "TIPO",        limit: 2
  end

  create_table "tmatchlg", id: false, force: :cascade do |t|
    t.integer "Id_Lay",         limit: 2,  null: false
    t.integer "Nivel_Filtro",   limit: 2,  null: false
    t.integer "Num_Condicion",  limit: 2,  null: false
    t.string  "Id_Campo_Izq",   limit: 20
    t.integer "Op_Arit_Izq",    limit: 2
    t.string  "Id_Campo_Izq_C", limit: 50
    t.integer "Cve_Operador",   limit: 2
    t.string  "Id_Campo_Der",   limit: 20
    t.integer "Op_Arit_Der",    limit: 2
    t.string  "Id_Campo_Der_C", limit: 50
    t.string  "Descripcion",    limit: 60
    t.integer "Long_Match_Izq", limit: 2
    t.string  "Plantilla_Izq",  limit: 50
    t.integer "Long_Match_Der", limit: 2
    t.string  "Plantilla_Der",  limit: 50
    t.integer "Cve_Conector",   limit: 2
  end

  add_index "tmatchlg", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "tmatchlg", ["Nivel_Filtro"], name: "PNivel_Filtro", using: :btree
  add_index "tmatchlg", ["Num_Condicion"], name: "PNum_Condicion", using: :btree

  create_table "tmsgmail", primary_key: "Id_Email", force: :cascade do |t|
    t.string "Direccion", limit: 60
    t.string "Nombre",    limit: 50
  end

  create_table "tmsgtctrl", id: false, force: :cascade do |t|
    t.integer "Id_Lay",                 limit: 4,  default: 0, null: false
    t.integer "Id_Formato",             limit: 4,  default: 0, null: false
    t.integer "Periodo",                limit: 4
    t.integer "UnidadTiempo",           limit: 4
    t.string  "UltimoArchivoEliminado", limit: 50
    t.integer "Activo",                 limit: 4
  end

  create_table "tmtokens", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.string  "Id_Token",    limit: 5,  null: false
    t.integer "Long_Id",     limit: 2
    t.integer "Long_Total",  limit: 2
    t.integer "Num_Campos",  limit: 2
    t.string  "Descripcion", limit: 50
  end

  create_table "tobjxapl", id: false, force: :cascade do |t|
    t.integer "Id_Aplicacion", limit: 2,  null: false
    t.integer "Id_Objeto",     limit: 2,  null: false
    t.string  "Descripcion",   limit: 50
    t.string  "Referencia",    limit: 30
    t.string  "Container",     limit: 25, null: false
  end

  add_index "tobjxapl", ["Container"], name: "PContainer", using: :btree

  create_table "topsxdet", id: false, force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,   null: false
    t.integer "Columna",      limit: 2,   null: false
    t.integer "Tipo_Campo",   limit: 2
    t.integer "Id_Campo_Ops", limit: 2,   null: false
    t.string  "Valor_Campo",  limit: 200
  end

  add_index "topsxdet", ["Columna"], name: "PColumna", using: :btree
  add_index "topsxdet", ["Id_Campo_Ops"], name: "PId_Campo_Ops", using: :btree
  add_index "topsxdet", ["Id_Lay"], name: "PId_Lay", using: :btree

  create_table "topsxgpo", id: false, force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,   null: false
    t.integer "Num_Nivel",    limit: 2,   null: false
    t.integer "Enc_o_Pie",    limit: 2,   null: false
    t.integer "Columna",      limit: 2,   null: false
    t.integer "Tipo_Campo",   limit: 2
    t.integer "Id_Campo_Ops", limit: 2,   null: false
    t.string  "Valor_Campo",  limit: 200
  end

  add_index "topsxgpo", ["Columna"], name: "PColumna", using: :btree
  add_index "topsxgpo", ["Enc_o_Pie"], name: "PEnc_o_Pie", using: :btree
  add_index "topsxgpo", ["Id_Campo_Ops"], name: "PId_Campo_Ops", using: :btree
  add_index "topsxgpo", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "topsxgpo", ["Num_Nivel"], name: "PNum_Nivel", using: :btree

  create_table "topsxlay", id: false, force: :cascade do |t|
    t.integer "Id_Lay",         limit: 2,   null: false
    t.integer "Num_Nivel",      limit: 2,   null: false
    t.integer "Id_Campo_Ops",   limit: 2,   null: false
    t.integer "Tipo_Campo",     limit: 2
    t.integer "Id_Campo",       limit: 2,   null: false
    t.string  "Auxiliar_Campo", limit: 200
  end

  add_index "topsxlay", ["Id_Campo_Ops"], name: "PId_Campo_Ops", using: :btree
  add_index "topsxlay", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "topsxlay", ["Num_Nivel"], name: "PNum_Nivel", using: :btree

  create_table "tparagen", primary_key: "Clave", force: :cascade do |t|
    t.string  "Descripcion", limit: 30
    t.integer "ValorNum",    limit: 4
    t.string  "ValorTxt",    limit: 50
  end

  add_index "tparagen", ["Clave"], name: "PClave", using: :btree

  create_table "tpatrones", id: false, force: :cascade do |t|
    t.integer "Id_Condicion", limit: 2,    null: false
    t.integer "Id_Horario",   limit: 2,    null: false
    t.integer "Id_Patron",    limit: 4,    null: false
    t.integer "Id_Campo",     limit: 2
    t.integer "Operador",     limit: 2
    t.integer "Monto",        limit: 4,    null: false
    t.string  "Valores",      limit: 2000, null: false
  end

  create_table "tpatronesestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",   limit: 12,               null: false
    t.integer "Id_Condicion", limit: 2,                null: false
    t.integer "Id_Horario",   limit: 2,                null: false
    t.integer "Nivel",        limit: 2,    default: 0, null: false
    t.integer "Id_Campo",     limit: 2,                null: false
    t.integer "Operador",     limit: 2,    default: 0
    t.integer "Monto",        limit: 4,    default: 0
    t.string  "Valores",      limit: 2000,             null: false
  end

  create_table "tradlocr", primary_key: "Id_Localizador", force: :cascade do |t|
    t.string "Telefono",    limit: 12
    t.string "Clave",       limit: 20
    t.string "Nombre",      limit: 50
    t.string "ServidorMsg", limit: 50
  end

  create_table "tradxala", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12, null: false
    t.integer "Id_Condicion",   limit: 2,  null: false
    t.integer "Id_Horario",     limit: 2,  null: false
    t.integer "Num_Alarma",     limit: 2,  null: false
    t.integer "Id_Localizador", limit: 2,  null: false
  end

  create_table "tradxalaestacion", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12, null: false
    t.integer "Id_Condicion",   limit: 2,  null: false
    t.integer "Id_Horario",     limit: 4,  null: false
    t.integer "Num_Alarma",     limit: 2,  null: false
    t.integer "Id_Localizador", limit: 2,  null: false
  end

  create_table "tradxalaxxx", id: false, force: :cascade do |t|
    t.string  "Id_Usuario",     limit: 12, null: false
    t.integer "Id_Condicion",   limit: 2,  null: false
    t.integer "Num_Alarma",     limit: 2,  null: false
    t.integer "Id_Localizador", limit: 2,  null: false
  end

  create_table "trelconfig", id: false, force: :cascade do |t|
    t.integer "Id_Config", limit: 4, null: false
    t.integer "Id_Grupo",  limit: 4, null: false
    t.integer "Id_Equipo", limit: 4, null: false
  end

  add_index "trelconfig", ["Id_Config"], name: "PId_Config", using: :btree
  add_index "trelconfig", ["Id_Equipo"], name: "PId_Equipo", using: :btree
  add_index "trelconfig", ["Id_Grupo"], name: "PId_Grupo", using: :btree

  create_table "ttiporegalarma", primary_key: "Id_TipoReg", force: :cascade do |t|
    t.string "Id_Descripcion", limit: 50
  end

  create_table "ttkneqxc", id: false, force: :cascade do |t|
    t.integer "Id_Lay",      limit: 2,  null: false
    t.string  "Id_Token",    limit: 10, null: false
    t.integer "Id_Campo",    limit: 2,  null: false
    t.integer "Consecutivo", limit: 2,  null: false
    t.integer "Longitud",    limit: 2
    t.string  "Valor",       limit: 50
    t.string  "Equivalente", limit: 50
  end

  add_index "ttkneqxc", ["Consecutivo"], name: "PConsecutivo", using: :btree
  add_index "ttkneqxc", ["Id_Campo"], name: "PId_Campo", using: :btree
  add_index "ttkneqxc", ["Id_Lay"], name: "PId_Lay", using: :btree
  add_index "ttkneqxc", ["Id_Token"], name: "PId_Token", using: :btree

  create_table "ttknmsgt", id: false, force: :cascade do |t|
    t.integer "Id_Lay",       limit: 2,  null: false
    t.integer "Id_Formato",   limit: 2,  null: false
    t.string  "Id_Token",     limit: 5,  null: false
    t.integer "Id_Campo",     limit: 2,  null: false
    t.integer "Tipo_Dato",    limit: 2
    t.integer "Longitud",     limit: 2
    t.string  "Nombre_Campo", limit: 50
  end

  create_table "ttknxusr", id: false, force: :cascade do |t|
    t.integer "Id_Lay",     limit: 2,  null: false
    t.integer "Id_Formato", limit: 2,  null: false
    t.string  "Id_Usuario", limit: 12, null: false
    t.string  "Id_Token",   limit: 5,  null: false
    t.integer "Id_Campo",   limit: 2,  null: false
  end

  create_table "tuseradm", id: false, force: :cascade do |t|
    t.string  "Id_Usuario", limit: 12, null: false
    t.string  "Nombre",     limit: 50
    t.string  "Puesto",     limit: 30
    t.string  "Telefono",   limit: 25
    t.string  "Ubicacion",  limit: 50
    t.integer "Tipo",       limit: 2
    t.string  "Contrasena", limit: 60
  end

  add_index "tuseradm", ["Id_Usuario"], name: "PId_usuario", using: :btree

  create_table "tuserskm", primary_key: "Id_Usuario", force: :cascade do |t|
    t.integer  "Id_Grupo",         limit: 2,  null: false
    t.integer  "Estado",           limit: 2
    t.integer  "Filtra_Evento",    limit: 2
    t.integer  "Ultima_Accion",    limit: 2
    t.datetime "Fec_Vig_Usuario"
    t.datetime "Fec_Vig_Clave"
    t.datetime "Fec_Hora_Ult_Acc"
    t.datetime "Fec_Hora_Ult_Opr"
    t.string   "Contrasena",       limit: 60
    t.string   "Nombre",           limit: 60
    t.string   "Ultima_Operacion", limit: 50
    t.datetime "Fec_Ini_Usuario"
    t.datetime "Fec_Ini_Clave"
  end

  add_index "tuserskm", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tuserskm", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "tuserskmbk", primary_key: "Id_Usuario", force: :cascade do |t|
    t.integer  "Id_Grupo",         limit: 2,  null: false
    t.integer  "Estado",           limit: 2
    t.integer  "Filtra_Evento",    limit: 2
    t.integer  "Ultima_Accion",    limit: 2
    t.datetime "Fec_Vig_Usuario"
    t.datetime "Fec_Vig_Clave"
    t.datetime "Fec_Hora_Ult_Acc"
    t.datetime "Fec_Hora_Ult_Opr"
    t.string   "Contrasena",       limit: 60
    t.string   "Nombre",           limit: 60
    t.string   "Ultima_Operacion", limit: 50
    t.datetime "Fec_Ini_Usuario"
    t.datetime "Fec_Ini_Clave"
  end

  add_index "tuserskmbk", ["Id_Grupo"], name: "PId_Grupo", using: :btree
  add_index "tuserskmbk", ["Id_Usuario"], name: "PId_Usuario", using: :btree

  create_table "twindaut", primary_key: "Id_Aplicacion", force: :cascade do |t|
    t.text "Mascara_Windows", limit: 4294967295
  end

  add_index "twindaut", ["Id_Aplicacion"], name: "PId_Aplicacion", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "tconalarfiltros", "tlaymsgt", column: "Id_Formato", primary_key: "Id_Formato", name: "tconalarfiltros_ibfk_1", on_delete: :cascade
  add_foreign_key "tconalarfiltros", "tlaymsgt", column: "Id_Lay", primary_key: "Id_Lay", name: "tconalarfiltros_ibfk_1", on_delete: :cascade
  add_foreign_key "tdetdconfiltros", "tconalarfiltros", column: "Num_Filtro", primary_key: "Num_Filtro", name: "tdetdconfiltros_ibfk_1", on_delete: :cascade
end
