require 'test_helper'

class InformaControllerTest < ActionController::TestCase
  test "should get consulta" do
    get :consulta
    assert_response :success
  end

  test "should get comentarios" do
    get :comentarios
    assert_response :success
  end

end
